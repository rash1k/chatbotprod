package com.portablepixels.smokefree.chatbot.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IncomingMessage extends Message {

    @SerializedName("quick_replies")
    @Expose
    private List<QuickReply> quickReplies;

    @SerializedName("sent_timestamp")
    private long sentTimestamp;

    @SerializedName("to")
    private String receiverParseId;


    public IncomingMessage(String receiverParseId, String user, long sentTimestamp) {
        super(receiverParseId, user);
        this.receiverParseId = receiverParseId;
        this.sentTimestamp = sentTimestamp;
    }

    public IncomingMessage(String randomId, UserProfile userProfile, String text) {
        super(randomId, userProfile, text);
    }

    public List<QuickReply> getQuickReplies() {
        return quickReplies;
    }


    public void setQuickReplies(List<QuickReply> quickReplies) {
        this.quickReplies = quickReplies;
    }

    public Long getSentTimestamp() {
        return sentTimestamp;
    }

    public void setSentTimestamp(Long sentTimestamp) {
        this.sentTimestamp = sentTimestamp;
    }

    public String getReceiverParseId() {
        return receiverParseId;
    }

    public void setReceiverParseId(String receiverParseId) {
        this.receiverParseId = receiverParseId;
    }
}
