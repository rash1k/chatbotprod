package com.portablepixels.smokefree.chatbot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutgoingHelloMessage extends Message {

    @SerializedName("user_profile")
    @Expose
    public UserProfile userProfile;

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
