package com.portablepixels.smokefree.chatbot;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.google.android.flexbox.FlexboxLayout;
import com.portablepixels.smokefree.R;
import com.portablepixels.smokefree.chatbot.model.IncomingMessage;
import com.portablepixels.smokefree.chatbot.model.Message;
import com.portablepixels.smokefree.chatbot.utils.AppUtils;
import com.portablepixels.smokefree.chatbot.utils.MessageUtils;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.udevel.widgetlab.TypingIndicatorView;

import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;


public class MessagesFragment extends BaseMessageFragment implements
        MessageInput.InputListener,
        ChatBot.OnMessageListener {

    //Message.OnChipListener,
    //MessageInput.AttachmentsListener,

    public static final String TAG = "MessageFragment";
    public static final String TEMP_USER_NAME = "9ooQeGTQrG";
//    public static final String TEMP_USER_ID = TEMP_USER_NAME;
    public static final String TEMP_USER_ID = "1";

    private MessagesList mMessagesList;
    private MessageInput mInput;
    private ChipCloud mChipCloud;
    private FlexboxLayout mFlexBoxLayout;
    private TypingIndicatorView mTypingIndicatorView;


    private ChatBot mChatBot;

    public static Fragment newInstance() {
        return new MessagesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_styled_messages, container, false);
        mTypingIndicatorView = view.findViewById(R.id.typingIndicator);

        mMessagesList = view.findViewById(R.id.messagesList);

        initMessageInput(view);
        initChipCloud(view);
        initAdapter();

        mChatBot = new ChatBot(this);

        return view;
    }


    private void initMessageInput(View view) {
        mInput = view.findViewById(R.id.input);
        hideKeyboardWhenMessageInputEmpty();
        mInput.setInputListener(this);
    }

    private void hideKeyboardWhenMessageInputEmpty() {
        EditText editText = mInput.getInputEditText();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    mInput.getInputEditText().onEditorAction(EditorInfo.IME_ACTION_DONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initChipCloud(View view) {
        mFlexBoxLayout = view.findViewById(R.id.flexbox);

        Resources res = getResources();

        ChipCloudConfig withCloseConfig = new ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.single)
                .checkedChipColor(res.getColor(R.color.selected_color))
                .checkedTextColor(res.getColor(R.color.selected_font_color))
                .uncheckedChipColor(res.getColor(R.color.accent_light_new))
                .uncheckedTextColor(res.getColor(R.color.white))
                .showClose(res.getColor(R.color.accent_dark), 500);

        mChipCloud = new ChipCloud(getContext(), mFlexBoxLayout, withCloseConfig);
    }


    @Override
    public boolean onSubmit(CharSequence input) {
        String text = input.toString();
        mMessagesAdapter.addToStart(MessageUtils.getTextMessage("1",
                TEMP_USER_NAME, text), true);

        sendOutgoingMessage(text);

        AppUtils.hideKeyboard(getActivity());
        showTypingIndicator();

        return true;
    }

    private void showTypingIndicator() {
        mTypingIndicatorView.setVisibility(View.VISIBLE);
    }



    @Override
    public void onMessage(final String json) {
        Log.d(TAG, "onMessage: " + json);
        final boolean isTypeMessage = MessageUtils.getMessageType(json);
        final IncomingMessage message = MessageUtils.createMessageFromJson(json);
        getActivity().runOnUiThread(() -> {
            if (message != null && isTypeMessage) {
                hideTypingIndicator();
                mMessagesAdapter.addToStart(message, true);
                showChipsCloud(message);
            }
        });
    }


    private void hideTypingIndicator() {
        mTypingIndicatorView.setVisibility(View.INVISIBLE);
    }


    private void initAdapter() {

        MessageHolders holdersConfig = new MessageHolders()
                .setOutcomingImageLayout(R.layout.item_custom_outcoming_image_message)
                .setIncomingTextLayout(R.layout.item_custom_incoming_text_message)
                .setOutcomingTextLayout(R.layout.item_custom_outcoming_text_message)
                .setIncomingImageLayout(R.layout.item_custom_incoming_image_message);


        super.mMessagesAdapter = new MessagesListAdapter<>(TEMP_USER_ID, holdersConfig, super.mImageLoader);
//        super.mMessagesAdapter = new MessagesListAdapter<>("1", holdersConfig, null);
        mMessagesList.setAdapter(super.mMessagesAdapter);
//        super.mMessagesAdapter.setLoadMoreListener(this);
//        super.mMessagesAdapter.enableSelectionMode(this);
//        super.mMessagesAdapter.setDateHeadersFormatter(this);
    }

    private void showChipsCloud(final IncomingMessage message) {
        String[] chipsArrayLabel = MessageUtils.getChipsArrayLabel(message);
        if (chipsArrayLabel != null && chipsArrayLabel.length > 0) {
            mChipCloud.addChips(chipsArrayLabel);
        }


        mChipCloud.setDeleteListener((index, label) -> {
            Log.d(TAG, String.format("chipDeleted at index:%d  label:%s ", index, label));
            sendOutgoingQuickMessage(message, index, Message.TYPE_MESSAGE);

            mMessagesAdapter.addToStart(MessageUtils.getTextMessage("1", TEMP_USER_NAME,
                    label), true);

            mChipCloud.deselectIndex(index);
            deleteAllChipsWithAnimation();
            showTypingIndicator();
        });
    }

    private void deleteAllChipsWithAnimation() {
        final FlexboxLayout parent = mFlexBoxLayout;
        AlphaAnimation anim = new AlphaAnimation(1.0F, 0.0F);
        anim.setDuration(500);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (parent.getChildCount() > 0) {
                    parent.removeAllViews();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        parent.startAnimation(anim);
    }

    private void sendOutgoingMessage(String text) {
        if (mChatBot.isOpenWebSocket()) {
            mChatBot.sendMessage(MessageUtils.createOutgoingMessage(TEMP_USER_NAME,
                    TEMP_USER_ID, text));
        } else {
            mChatBot.connectToWebSocket();
            if (mChatBot.isOpenWebSocket()) {
                mChatBot.sendMessage(MessageUtils.createOutgoingMessage(TEMP_USER_NAME,
                        TEMP_USER_ID, text));
            }
        }

    }

    private void sendOutgoingQuickMessage(IncomingMessage message, int index, String messageType) {
        if (mChatBot.isOpenWebSocket()) {
            mChatBot.sendMessage(MessageUtils.createOutgoingQuickMessage(message, index));
        } else {
            mChatBot.connectToWebSocket();
            if (mChatBot.isOpenWebSocket()) {
                mChatBot.sendMessage(MessageUtils.createOutgoingQuickMessage(message, index));
            }
        }
    }
}




