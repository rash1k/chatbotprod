package com.portablepixels.smokefree.chatbot.utils;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.portablepixels.smokefree.chatbot.model.ChatbotModel;
import com.portablepixels.smokefree.chatbot.model.IncomingMessage;
import com.portablepixels.smokefree.chatbot.model.Message;
import com.portablepixels.smokefree.chatbot.model.OutgoingHelloMessage;
import com.portablepixels.smokefree.chatbot.model.OutgoingMessage;
import com.portablepixels.smokefree.chatbot.model.OutgoingQuickMessage;
import com.portablepixels.smokefree.chatbot.model.QuickReply;
import com.portablepixels.smokefree.chatbot.model.UserProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;


public class MessageUtils {

    public static String createOutgoingHelloMessage(String userName, String userId) {
        OutgoingHelloMessage outgoingMessage = new OutgoingHelloMessage();
        outgoingMessage.setType("hello");
        outgoingMessage.setUserStr(userName);
        outgoingMessage.setChannel("socket");

        UserProfile userProfile = createUserProfile(userId);
        outgoingMessage.setUserProfile(userProfile);
        return new Gson().toJson(outgoingMessage);
    }

    private static UserProfile createUserProfile(@Nullable String userId) {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        formatter.setLenient(true);
        String dateStr = formatter.format(date);

        UserProfile user = new UserProfile();
        user.setQuitDate(dateStr);
        user.setTimeSmokeFree("timeSmokeFree");
        user.setMoneySaved("moneySaved");
        user.setBadgesEarned("badgesEarned");
        user.setCravingsResisted("cravingsResisted");
        user.setCravingsResistedRecently("cravingsResistedRecently");
        user.setNotSmoked("notSmoked");
        user.setNotSmokedRecently("notSmokedRecently");
        userId = userId != null ? userId : "9ooQeGTQrG";
        user.setId(userId);
        user.setTimezoneOffset("300");

        return user;
    }

    public static String createOutgoingMessage(String userName, String userId, String messageText) {
        OutgoingMessage message = new OutgoingMessage();

        message.setType("message");
        if (messageText != null) {
            message.setText(messageText);
        }
        userName = userName != null ? userName : "9ooQeGTQrG";
        message.setUserStr(userName);
        message.setChannel("socket");


        return new Gson().toJson(message);
    }

    public static String createOutgoingQuickMessage(IncomingMessage message, int index) {
        OutgoingQuickMessage outgoingMessage = new OutgoingQuickMessage();

        outgoingMessage.setType("message");
        outgoingMessage.setText(MessageUtils.getStringTitledByIndex(message, index));
        outgoingMessage.setPayload(MessageUtils.getStringPayloadByIndex(message, index));
        outgoingMessage.setUserStr(message.getUserStr());
        outgoingMessage.setChannel(message.getChannel());

        return new Gson().toJson(outgoingMessage);
    }

    public static IncomingMessage createMessageFromJson(String json) {
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(json);
            if (jsonObject.optString("type").equals("typing")) {
                return null;
            }
            String to = jsonObject.optString("to");
            String user = jsonObject.optString("user");
            long timeStamp = jsonObject.optLong("sent_timestamp");

            IncomingMessage message = new IncomingMessage(to, user, timeStamp);
            if (timeStamp != 0) message.setCreatedAt(new Date(timeStamp));
            else message.setCreatedAt(new Date());

//            Message message = new Message(to, user);
            message.setType(jsonObject.getString("type"));

            if (message.getType().equals(ChatbotModel.TYPE_MESSAGE)) {
                message.setText(jsonObject.getString("text"));
                message.setChannel(jsonObject.optString("channel"));

                JSONArray files = jsonObject.optJSONArray("files");
                if (files != null && files.length() > 0) {
                    JSONObject firstElementFiles = files.getJSONObject(0);
                    message.setImage(new Message.Image(firstElementFiles.getString("url")));
                }

                JSONArray quickReplies = jsonObject.optJSONArray("quick_replies");
                if (quickReplies != null) {
                    int length = quickReplies.length();
                    List<QuickReply> replies = new ArrayList<>(length);

                    for (int i = 0; i < length; i++) {
                        JSONObject jsonObject1 = quickReplies.getJSONObject(i);
                        String payload = jsonObject1.getString("payload");
                        String title = jsonObject1.getString("title");

                        QuickReply quickReply = new QuickReply();
                        quickReply.setTitle(title);
                        quickReply.setPayload(payload);

                        replies.add(quickReply);
                    }
                    message.setQuickReplies(replies);
                }

                return message;
            } else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }



    public static Message getTextMessage(String userId, String userName, String text) {
        return new IncomingMessage(getRandomId(), getUserProfile(userId, userName), text);
    }

    public static UserProfile getUserProfile(String userId, String userName) {
        return new UserProfile(
                userId,
                userName, null,
                true);
    }


    public static String getResourceNameById(Context ctx, @IdRes int idResource) {
        return ctx.getResources().getResourceEntryName(idResource);
    }

    public static String getStringPayloadByIndex(IncomingMessage message, int index) {
        List<QuickReply> quickReplies = message.getQuickReplies();

        QuickReply quickReply = quickReplies.get(index);
        return quickReply.getPayload();
    }

    public static String getStringTitledByIndex(IncomingMessage message, int index) {
        List<QuickReply> quickReplies = message.getQuickReplies();

        QuickReply quickReply = quickReplies.get(index);
        return quickReply.getTitle();
    }

    public static String[] getChipsArrayLabel(IncomingMessage message) {
        List<QuickReply> quickReplies = message.getQuickReplies();
        if (quickReplies == null) return null;
        int length = quickReplies.size();
        String[] arrayLabel = new String[length];

        for (int i = 0; i < quickReplies.size(); i++) {
            QuickReply quickReply = quickReplies.get(i);
            arrayLabel[i] = quickReply.getTitle();
        }
        return arrayLabel;
    }

    public static boolean getMessageType(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            String typeMessage = jsonObject.optString("type");

            return typeMessage != null &&
                    typeMessage.equals(ChatbotModel.TYPE_MESSAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getRandomId() {
        return Long.toString(UUID.randomUUID().getLeastSignificantBits());
    }

}
