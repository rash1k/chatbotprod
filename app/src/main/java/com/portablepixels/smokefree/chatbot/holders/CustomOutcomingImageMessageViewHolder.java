package com.portablepixels.smokefree.chatbot.holders;

import android.view.View;

import com.portablepixels.smokefree.chatbot.model.Message;
import com.stfalcon.chatkit.messages.MessageHolders;


/*
 * Created by troy379 on 05.04.17.
 */
public class CustomOutcomingImageMessageViewHolder
        extends MessageHolders.OutcomingImageMessageViewHolder<Message> {

    public CustomOutcomingImageMessageViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(Message message) {
        super.onBind(message);

        time.setText(message.getStatus() + " " + time.getText());
    }
}