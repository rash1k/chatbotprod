package com.portablepixels.smokefree.chatbot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.stfalcon.chatkit.commons.models.IUser;


public class User implements IUser {
    @SerializedName("quitDate")
    @Expose
    private String quitDate;
    @SerializedName("timeSmokeFree")
    @Expose
    private String timeSmokeFree;
    @SerializedName("moneySaved")
    @Expose
    private String moneySaved;
    @SerializedName("badgesEarned")
    @Expose
    private String badgesEarned;
    @SerializedName("cravingsResisted")
    @Expose
    private String cravingsResisted;
    @SerializedName("cravingsResistedRecently")
    @Expose
    private String cravingsResistedRecently;
    @SerializedName("notSmoked")
    @Expose
    private String notSmoked;
    @SerializedName("notSmokedRecently")
    @Expose
    private String notSmokedRecently;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("timezone_offset")
    @Expose
    private String timezoneOffset;

   transient private String name;

   transient private String avatar;

   transient private boolean online;

    public User() {
    }

    public User(String id, String name, String avatar, boolean online) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.online = online;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getQuitDate() {
        return quitDate;
    }

    public void setQuitDate(String quitDate) {
        this.quitDate = quitDate;
    }

    public String getTimeSmokeFree() {
        return timeSmokeFree;
    }

    public void setTimeSmokeFree(String timeSmokeFree) {
        this.timeSmokeFree = timeSmokeFree;
    }

    public String getMoneySaved() {
        return moneySaved;
    }

    public void setMoneySaved(String moneySaved) {
        this.moneySaved = moneySaved;
    }

    public String getBadgesEarned() {
        return badgesEarned;
    }

    public void setBadgesEarned(String badgesEarned) {
        this.badgesEarned = badgesEarned;
    }

    public String getCravingsResisted() {
        return cravingsResisted;
    }

    public void setCravingsResisted(String cravingsResisted) {
        this.cravingsResisted = cravingsResisted;
    }

    public String getCravingsResistedRecently() {
        return cravingsResistedRecently;
    }

    public void setCravingsResistedRecently(String cravingsResistedRecently) {
        this.cravingsResistedRecently = cravingsResistedRecently;
    }

    public String getNotSmoked() {
        return notSmoked;
    }

    public void setNotSmoked(String notSmoked) {
        this.notSmoked = notSmoked;
    }

    public String getNotSmokedRecently() {
        return notSmokedRecently;
    }

    public void setNotSmokedRecently(String notSmokedRecently) {
        this.notSmokedRecently = notSmokedRecently;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatar() {
        return avatar;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimezoneOffset() {
        return timezoneOffset;
    }

    public void setTimezoneOffset(String timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }
}