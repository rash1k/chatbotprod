package com.portablepixels.smokefree.chatbot;


import android.util.Log;

import com.portablepixels.smokefree.BuildConfig;
import com.portablepixels.smokefree.chatbot.utils.MessageUtils;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URISyntaxException;


public class ChatBot {

    private static final String TAG = "ChatBot";

    private ChatBotClient client;


    public ChatBot(OnMessageListener listener) {
        try {
            this.client = new ChatBotClient(listener);
            client.connect();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        client.send(message);
    }


    public boolean isOpenWebSocket() {
        return client.isOpen();
    }

    public void connectToWebSocket() {
        client.connect();
    }


    private static class ChatBotClient extends WebSocketClient {

        private WeakReference<OnMessageListener> mOnMessageListener;

        public ChatBotClient(OnMessageListener listener) throws URISyntaxException {
            super(new URI(BuildConfig.CHATBOT_URL));
            mOnMessageListener = new WeakReference<>(listener);
        }


        @Override
        public void onOpen(ServerHandshake serverHandshake) {
            Log.d(TAG, "Chatbot websocket is Open");



            //Why does not it work?
            String helloMessage = MessageUtils.createOutgoingHelloMessage("9ooQeGTQrG", "9ooQeGTQrG");
            send(helloMessage);


//            String message = MessageUtils.createOutgoingMessage("9ooQeGTQrG", "9ooQeGTQrG", "Hello");
//            send(message);


          /*  //Hello message not work ?
            send("{\n" +
                    "  \"type\": \"hello\",\n" +
                    "  \"user\": \"9ooQeGTQrG\",\n" +
                    "  \"channel\": \"socket\",\n" +
                    "  \"user_profile\": {\n" +
                    "    \"quitDate\": \"2018-06-19\",\n" +
                    "    \"timeSmokeFree\": \"timeSmokeFree\",\n" +
                    "    \"moneySaved\": \"moneySaved\",\n" +
                    "    \"badgesEarned\": \"badgesEarned\",\n" +
                    "    \"cravingsResisted\": \"cravingsResisted\",\n" +
                    "    \"cravingsResistedRecently\": \"cravingsResistedRecently\",\n" +
                    "    \"notSmoked\": \"notSmoked\",\n" +
                    "    \"notSmokedRecently\": \"notSmokedRecently\",\n" +
                    "    \"id\": \"9ooQeGTQrG\",\n" +
                    "    \"timezone_offset\": 300\n" +
                    "  }\n" +
                    "}\n");*/
        }

        @Override
        public void onMessage(String s) {
            Log.d(TAG, "ChatBot onMessage: " + s);
            if (mOnMessageListener != null) {
                mOnMessageListener.get().onMessage(s);
            }
        }

        @Override
        public void onClose(int i, String s, boolean b) {
            Log.d(TAG, "websocket onClose");

        }

        @Override
        public void onError(Exception e) {
            Log.e(TAG, "ChatBot onError");
            e.printStackTrace();
        }

    }

    public interface OnMessageListener {
        void onMessage(String s);
    }
}
