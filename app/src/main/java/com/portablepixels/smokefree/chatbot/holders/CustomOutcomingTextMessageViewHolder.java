package com.portablepixels.smokefree.chatbot.holders;

import android.view.View;

import com.portablepixels.smokefree.chatbot.model.Message;
import com.stfalcon.chatkit.messages.MessageHolders;


public class CustomOutcomingTextMessageViewHolder
        extends MessageHolders.OutcomingTextMessageViewHolder<Message> {

//    private ChipCloud mChipCloud;


    public CustomOutcomingTextMessageViewHolder(View itemView) {
        super(itemView);

//        mChipCloud = itemView.findViewById(R.id.chip_cloud);
//        mChipCloud.addChip("Hello");
    }

    @Override
    public void onBind(final Message message) {
        super.onBind(message);

        time.setText(message.getStatus() + " " + time.getText());

//        boolean isTypeMessage = MessageUtils.getMessageType(json);
        /*String[] chipsArrayLabel = MessageUtils.getChipsArrayLabel(message);
        if (chipsArrayLabel != null && chipsArrayLabel.length > 0) {
            mChipCloud.addChips(chipsArrayLabel);
        }

        mChipCloud.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int i) {
                String label = MessageUtils.getStringPayloadByIndex(message,i);
                Message.OnChipListener chipListener = message.getChipListener();
                if (chipListener != null) chipListener.onChipListener(label);
            }

            @Override
            public void chipDeselected(int i) {

            }
        });
*/
    }
}
