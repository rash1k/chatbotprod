package com.portablepixels.smokefree.chatbot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatBotModelResponse {

    public static class CaptureOptions {

        @SerializedName("key")
        @Expose
        private String key;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

    }


    public static class File {

        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("image")
        @Expose
        private boolean image;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public boolean isImage() {
            return image;
        }

        public void setImage(boolean image) {
            this.image = image;
        }

    }


    public static class Handler {

        @SerializedName("pattern")
        @Expose
        private String pattern;
        @SerializedName("default")
        @Expose
        private boolean _default;

        public String getPattern() {
            return pattern;
        }

        public void setPattern(String pattern) {
            this.pattern = pattern;
        }

        public boolean isDefault() {
            return _default;
        }

        public void setDefault(boolean _default) {
            this._default = _default;
        }

    }


    public static class MessageResponse {

        @SerializedName("quick_replies")
        @Expose
        private List<QuickReply> quickReplies = null;

        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("files")
        @Expose
        private List<File> files = null;
        @SerializedName("payload")
        @Expose
        private String payload;
        @SerializedName("channel")
        @Expose

        private String channel;
        @SerializedName("capture_options")
        @Expose
        private CaptureOptions captureOptions;
        @SerializedName("handler")
        @Expose
        private List<Handler> handler = null;
        @SerializedName("sent_timestamp")
        @Expose
        private int sentTimestamp;
        @SerializedName("user")
        @Expose
        private String user;
        @SerializedName("to")
        @Expose
        private String to;
        @SerializedName("type")
        @Expose
        private String type;

        public List<QuickReply> getQuickReplies() {
            return quickReplies;
        }

        public void setQuickReplies(List<QuickReply> quickReplies) {
            this.quickReplies = quickReplies;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public List<File> getFiles() {
            return files;
        }

        public void setFiles(List<File> files) {
            this.files = files;
        }


        public String getPayload() {
            return payload;
        }

        public void setPayload(String payload) {
            this.payload = payload;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public CaptureOptions getCaptureOptions() {
            return captureOptions;
        }

        public void setCaptureOptions(CaptureOptions captureOptions) {
            this.captureOptions = captureOptions;
        }

        public List<Handler> getHandler() {
            return handler;
        }

        public void setHandler(List<Handler> handler) {
            this.handler = handler;
        }

        public int getSentTimestamp() {
            return sentTimestamp;
        }

        public void setSentTimestamp(int sentTimestamp) {
            this.sentTimestamp = sentTimestamp;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }


    public static class QuickReply {

        @SerializedName("content_type")
        @Expose
        private String contentType;
        @SerializedName("payload")
        @Expose
        private String payload;
        @SerializedName("title")
        @Expose
        private String title;

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        public String getPayload() {
            return payload;
        }

        public void setPayload(String payload) {
            this.payload = payload;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

}
