package com.portablepixels.smokefree.chatbot.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public abstract class ChatbotModel {

    private static final String TAG = "ChatbotModel";
    /**
     * Messages types
     **/

    public static final String TYPE_MESSAGE = "message";
    static final String TYPE_TYPING = "typing";
    static final String TYPE_HELLO = "hello";

    /** END Messages types **/

    /**
     * Content types
     **/

    static final String CONTENT_TYPE_TEXT = "text";
    static final String CONTENT_TYPE_PICTURE = "picture"; // TODO get real
    static final String CONTENT_TYPE_GIF = "gif"; // TODO get real

    /**
     * END Content types
     **/

    public static class Message {
        @SerializedName("type")
        public String type;

        @SerializedName("user")
        public String parseUserID;

        @SerializedName("text")
        public String text;

        @SerializedName("channel")
        public String channel = "socket";

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getParseUserID() {
            return parseUserID;
        }

        public void setParseUserID(String parseUserID) {
            this.parseUserID = parseUserID;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }
    }

    public static class UserModel {
        @SerializedName("id")
        public String parseUserID;

        @SerializedName("quitDate")
        public String quitDate;

        @SerializedName("timeSmokeFree")
        public String timeSmokeFree;

        @SerializedName("moneySaved")
        public String moneySaved;

        @SerializedName("cravingsResisted")
        public String cravingsResisted;

        @SerializedName("cravingsResistedRecently")
        public String cravingsResistedRecently;

        @SerializedName("notSmoked")
        public String notSmoked;

        @SerializedName("timezone_offset")
        public Integer timezoneOffset;

        public String getParseUserID() {
            return parseUserID;
        }

        public void setParseUserID(String parseUserID) {
            this.parseUserID = parseUserID;
        }

        public String getQuitDate() {
            return quitDate;
        }

        public void setQuitDate(String quitDate) {
            this.quitDate = quitDate;
        }

        public String getTimeSmokeFree() {
            return timeSmokeFree;
        }

        public void setTimeSmokeFree(String timeSmokeFree) {
            this.timeSmokeFree = timeSmokeFree;
        }

        public String getMoneySaved() {
            return moneySaved;
        }

        public void setMoneySaved(String moneySaved) {
            this.moneySaved = moneySaved;
        }

        public String getCravingsResisted() {
            return cravingsResisted;
        }

        public void setCravingsResisted(String cravingsResisted) {
            this.cravingsResisted = cravingsResisted;
        }

        public String getCravingsResistedRecently() {
            return cravingsResistedRecently;
        }

        public void setCravingsResistedRecently(String cravingsResistedRecently) {
            this.cravingsResistedRecently = cravingsResistedRecently;
        }

        public String getNotSmoked() {
            return notSmoked;
        }

        public void setNotSmoked(String notSmoked) {
            this.notSmoked = notSmoked;
        }

        public Integer getTimezoneOffset() {
            return timezoneOffset;
        }

        public void setTimezoneOffset(Integer timezoneOffset) {
            this.timezoneOffset = timezoneOffset;
        }
    }

    public static class HelloMessage {
        @SerializedName("user_profile")
        public UserModel userModel;

        public UserModel getUserModel() {
            return userModel;
        }

        public void setUserModel(UserModel userModel) {
            this.userModel = userModel;
        }
    }

    public class QuickMessage {
        @SerializedName("payload")
        public String payload;

        public String getPayload() {
            return payload;
        }

        public void setPayload(String payload) {
            this.payload = payload;
        }
    }

    public static class IncomingMessage {
        @SerializedName("quick_replies")
        public List<QuickReplyModel> listOfReplies;

        @SerializedName("sent_timestamp")
        public Long sentTimestamp;

        @SerializedName("to")
        public Long receiverParseID;

        public List<QuickReplyModel> getListOfReplies() {
            return listOfReplies;
        }

        public void setListOfReplies(List<QuickReplyModel> listOfReplies) {
            this.listOfReplies = listOfReplies;
        }

        public Long getSentTimestamp() {
            return sentTimestamp;
        }

        public void setSentTimestamp(Long sentTimestamp) {
            this.sentTimestamp = sentTimestamp;
        }

        public Long getReceiverParseID() {
            return receiverParseID;
        }

        public void setReceiverParseID(Long receiverParseID) {
            this.receiverParseID = receiverParseID;
        }
    }

    public static class QuickReplyModel {
        @SerializedName("content_type")
        public String contentType;

        @SerializedName("payload")
        public String payload;

        @SerializedName("title")
        public String title;

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        public String getPayload() {
            return payload;
        }

        public void setPayload(String payload) {
            this.payload = payload;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class Files {
        @SerializedName("url")
        public String url;

        @SerializedName("image")
        public boolean image;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public boolean isImage() {
            return image;
        }

        public void setImage(boolean image) {
            this.image = image;
        }
    }
}