package com.portablepixels.smokefree.chatbot;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.portablepixels.smokefree.R;
import com.portablepixels.smokefree.chatbot.model.Message;
import com.portablepixels.smokefree.chatbot.utils.AppUtils;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.Date;

public abstract class BaseMessageFragment extends Fragment
//        implements MessagesListAdapter.SelectionListener,
//        MessagesListAdapter.OnLoadMoreListener,
//        DateFormatter.Formatter
{


    private static final int TOTAL_MESSAGES_COUNT = 100;

    protected final String mSenderId = "0";
    protected ImageLoader mImageLoader;
    protected MessagesListAdapter<Message> mMessagesAdapter;

    private Menu menu;
    private int selectionCount;
    private Date lastLoadedDate;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mImageLoader = (imageView, url) -> {
            if (AppUtils.isStringHttpProtocol(url)) {
                Uri uri = Uri.parse(url);
                Picasso.get().load(uri).into(imageView);
            } /*else {
                Picasso.with(getContext())
                        .load(R.drawable.chat_bot)
                        .into(imageView);
            }*/
        };
    }

    @Override
    public void onResume() {
        super.onResume();
//        Appodeal.hide(getActivity(), Appodeal.BANNER_BOTTOM);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.chat_actions_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
     /*   switch (item.getItemId()) {
            case R.id.action_delete:
                mMessagesAdapter.deleteSelectedMessages();
                break;
            case R.id.action_copy:
                mMessagesAdapter.copySelectedMessagesText(Objects.requireNonNull(getContext()),
                        getMessageStringFormatter(), true);
                AppUtils.showToast(getContext(), R.string.copied_message, true);
                break;
        }*/
        return true;
    }
}
